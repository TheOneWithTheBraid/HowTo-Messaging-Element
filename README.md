# Repo einrichten

```
git clone https://codeberg.org/EG-HH/HowTo-Messaging-Element.git Element
cd Element
cp .githooks/post-checkout .git/hooks/post-checkout
cp .githooks/post-merge .git/hooks/post-merge
cp .githooks/post-commit .git/hooks/post-commit
git checkout master
```

# PDF erstellen

```
pdflatex Element.tex
pdflatex Element.tex
```

# HTML erstellen

```
htlatex Element "Element.cfg"
```

# Copyright

 Diese Anleitung ist gemeinfrei nach [Creative Commons CC0](https://creativecommons.org/publicdomain/zero/1.0/deed.de). Davon ausgenommen sind die Teile der Bildschirmfotos, die [Element](https://element.io) zeigen, welches unter der [Apache License 2.0](https://www.apache.org/licenses/LICENSE-2.0) lizensiert ist. Außerdem ausgenommen ist der verwendete CCS-Style [dark.css](dark.css) von [kognise](https://github.com/kognise), der unter der [MIT Lizenz](MIT.txt) steht.
